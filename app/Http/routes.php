<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// using named routes
Route::get('/', [
	'as' => 'homepage', 'uses' => 'PagesController@home'
]);

Route::get('contact', [
	'as' => 'contact page', 'uses' => 'PagesController@contact'
]);