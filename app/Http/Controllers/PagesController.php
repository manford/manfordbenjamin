<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    // route handling without named routes
    public function home()
    {
    	// return home view
    	return view('home');
    }

    public function contact()
    {
    	// return contact view
    	return view('contact');
    }

}
