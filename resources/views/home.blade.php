@extends('master')
@section('title', 'Homepage')

@section('content')

  <!-- Content -->
  <div class="main" role="main">
    <article class="post clear">

      <h1 class="post__title">
        Homepage
      </h1>
      <div class="post__meta">
        <a>
          Built with Laravel on June 11, 2016
        </a>
      </div>

      Hi there. Here is a list of bullet points that summarizes my existence:
      <ul>
        <li>I'm in search of my next big project, after i stopped freelancing.</li>
        <li>I <a>invest in</a> and like helping startups, both individually and as a part-time partner.</li>
        <li>I live in Accra, Ghana. I enjoy programming and building products.</li>
        <li>I'm currently working at <a>Callens Solutions Limited</a>.</li>
      </ul>

      <br>Projects
      <ul>
        <li>An application that lets you find info of any school in Ghana: <a><u>QLS Portal</u></a></li>
        <li>Others <a><u>Censored</u> ;)</a></li>
      </ul>

    </article>
  </div>
  <!-- Content -->

@endsection
