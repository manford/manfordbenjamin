<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="description" content="Software engineer helping build a better web.">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <link href="css/style.min.css" rel="stylesheet">
  </head>

  <body>

    <!-- Side bar -->
    @include('sidebar')
    <!-- Side Bar -->

    <!-- Content -->
    @yield('content')
    <!-- Content -->

    <script src="js/scripts.min.js"></script>

  </body>
</html>
