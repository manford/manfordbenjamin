<div class="sidebar clear">
    <div class="sidebar__profile">
	    <a href="/">
	      <img src="img/benjamin.jpg" alt="Benjamin Manford" class="sidebar__profile-img">
	    </a>
	    <h1 class="sidebar__profile-name">
	      Benjamin Manford
	    </h1>
	    <div class="sidebar__profile-bio clear">
	      <p>
	        Ghana bred. Software Engineer. Lead front-end engineer @ <a>Callens Solutions Limited</a>. JavaScript, 
	        <br>Html5, PHP, Python, Elixir, Erlang, Ruby. I hack anything that comes to mind as well.
	      </p>
	    </div>
	    <div class="sidebar__profile-social">
	      <a href="//twitter.com/manfordbenjamin" class="twitter-follow-button" data-show-count="true">
	        Follow @manfordbenjamin
	      </a>
	    </div>
	    <nav class="sidebar__profile-nav">
	      <ul>
	        <li>
	          <a href="/">
	            Homepage
	          </a>
	        </li>
	        <li>
	          <a href="/contact">
	            Contact Me
	          </a>
	        </li>
	        <li>
	          <a target="_blank" href="//www.facebook.com/iammanford">
	            Facebook
	          </a>
	        </li>
	        <li>
	          <a target="_blanck" href="//github.com/manfordbenjamin">
	            GitHub
	          </a>
	        </li>
	      </ul>
	    </nav>
    </div>
</div>