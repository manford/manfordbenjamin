@extends('master')
@section('title', 'Contact Me')

@section('content')

  <!-- Content -->
  <div class="main" role="main">
    <article class="post clear">

      <h1 class="post__title">
        Contact Me
      </h1>
      <div class="post__meta">
        <a>
          Built with Laravel on June 11, 2016
        </a>
      </div>

      <ul>
        <li>Email me at 
        <a href="mailto:benjamin.manford@callenssolutions.com">benjamin.manford@callenssolutions.com</a>.
        </li>   
        <li>Friend me on <a target="_blank" href="https://web.facebook.com/iammanford">Facebook</a>.</li>
        <li>Or follow me on <a target="_blank" href="https://twitter.com/manfordbenjamin">Twitter</a>.</li>
      </ul>
      
    </article>
  </div>
  <!-- Content -->

@endsection
